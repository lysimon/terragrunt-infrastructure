terragrunt = {
  terraform {
    source = "../../../modules//ecs-cluster"
  }

  include = {
    path = "${find_in_parent_folders()}"
  }

  dependencies {
    paths = ["../vpc"]
  }
}

# Module parameter
aws_region = "us-east-1"

